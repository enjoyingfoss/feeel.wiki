If you'd like to help out by processing photos (either your own or ones donated by the community) into low-poly images, you can do so easily with one of the apps listed below:

Apps for low-poly processing
---

**Use [FOSStriangulator](https://github.com/FOSStriangulator/FOSStriangulator) to process a photo into a low-poly image** ([Linux](https://flathub.org/apps/details/org.enjoyingfoss.FOSStriangulator), [Windows](https://www.microsoft.com/store/apps/9n6mf3dngr9q)).

On macOS, you can use the proprietary [DMesh](http://dmesh.thedofl.com/) app.

If using something not on this list, make sure it creates triangles with solid colors (not gradients!), allows custom point placement, and exports to a vector image (e.g. SVG).

How to process an image
---

The process for creating a low-poly image is described below. DMesh is used in the accompanying screenshots, but the process is the same for Image Triangulator.

2. Import a photo licensed licensed under one of these licenses: [CC0](https://creativecommons.org/choose/zero/), [CC BY](https://creativecommons.org/licenses/by/4.0/), or [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). The person in the photo should be fully in the frame, from head to toe.
3. Add points outlining the figure in your mesh editor
![Adding figure outlines in DMesh](https://gitlab.com/enjoyingfoss/feeel-legacy/raw/master/mesh-tutorial/Screencast%202018-01-15%20at%206.00.55%20PM.gif)
4. Add points outlining sharp changes within the figure. Don't outline facial features in much detail, though, so that the person performing the exercise maintains their anonymity.
![Adding element outlines in DMesh](https://gitlab.com/enjoyingfoss/feeel-legacy/raw/2ca8a668d283f8fc7c404e9e3e03bf122abdb527/mesh-tutorial/Screencast%202018-01-15%20at%206.03.07%20PM.gif)
5. Add points in the middle of objects, to prevent overly simplified shapes.
![Adding points in the middle in DMesh](https://gitlab.com/enjoyingfoss/feeel-legacy/raw/master/mesh-tutorial/Screencast%202018-01-15%20at%206.07.45%20PM.gif)
6. Export the mesh as a vector, including the points.
7. Upload your mesh somewhere and place a link in a comment under https://gitlab.com/enjoyingfoss/feeel/issues/2

(If your mesh contains the background, don't worry about it--background triangles can be removed later.)