You can rate and/or review Feeel on the following sites:

- [Google Play](https://play.google.com/store/apps/details?id=com.enjoyingfoss.feeel)
- [AlternativeTo](https://alternativeto.net/software/feeel/about/)
- [Slant](https://www.slant.co/topics/7818/viewpoints/5/~workout-apps-for-android~feeel)