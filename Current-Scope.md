Out of scope features
==============

Sets and reps
----------
Right now, Feeel is centered around workouts consisting of timed exercises. It doesn't currently work with sets and reps or with schedules — for that, use the free and open-source app [wger](https://wger.de/).

Custom exercises
--------
Feeel does not allow adding custom exercises privately. This is done for several reasons:

1. **Complications for future development:** For example, one planned feature for Feeel is importing, exporting, and sharing workouts. This would be relatively simple with built-in exercises, as they could easily be referred to with just exercise IDs. With custom exercises, this wouldn't work, and instead they would have to be saved to the workout itself — probably as just titles and descriptions, because saving exercise images too would complicate things a lot more. What you'd end up with is people sharing workouts with a variety of non-maintained and low-quality (= having no image and often no description) exercises, even if those exercises have been added to Feeel later on.
2. **Discouraging contribution:** Adding a custom exercise without contributing it to Feeel would solve the short-term problem of being able to add that exercise to a workout right away, but it could be destructive to Feeel in the long-term. If people create custom exercises without contributing them, Feeel won't build up a strong database of exercises, which is key to a successful workout app.
3. **Lots of extra development:** As with any feature, custom exercises would have to be implemented and maintained. Maintenance especially could be quite painful, as there would have to be extra considerations whenever dealing with exercises. The complications with workout sharing discussed in the first point here is one example. Another example is deleting custom exercises and what that would entail. Or finding exercise duplicates and weeding those out. At the time of this writing, Feeel has a single developer working on it in his spare time. Implementing and maintaining this feature would significantly reduce the already slow development pace of Feeel.

That said, it's true that currently Feeel doesn't have many exercises and that contributing them is not a one-click affair (though it's possible and highly encouraged — see [Contributing](Contributing) for details). Fortunately, [wger](https://github.com/wger-project/wger) is working on an exercise wiki that anyone will be able to easily contribute to. And Feeel will pull exercises from this wiki in the future.

For the time being, if you want to define your own exercises without contributing them to Feeel, you can use the open-source app [Workout Time](https://f-droid.org/en/packages/es.ideotec.workouttime/), which allows you to create workouts with your own exercises.