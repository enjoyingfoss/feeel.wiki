* [Home](home)
* [Contributing](Contributing)
  * [Processing photos](Processing-photos)
  * [App rating and reviews](App-rating-and-reviews)
* General info
  * [Current scope](Current-scope)