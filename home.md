Hey there!

This is a place for general information about Feeel. Refer to the sidebar on the right to find the page you're looking for. (On mobile devices and with smaller window sizes, you can show he sidebar using the « button.)

If you have any questions or comments, you can get in touch with the community on [Matrix](https://matrix.to/#/!jFShhgWHRXehKXrToU:matrix.org?via=matrix.org).